variable "fqdn" {
  type    = string
  default = "claudia.fejk.net"
}

variable "dcs" {
  type    = list(string)
  default = ["dc1", "devel"]
}

variable "image" {
  type = string
}

job "__JOB_NAME__" {
  datacenters = var.dcs

  group "frontend" {
    count = 1

    network {
      port "fe" { to = 80 }
    }

    service {
      name = "${JOB}-http"

      tags = [
        "public",
        "traefik.enable=true",
        "traefik.http.routers.${NOMAD_JOB_NAME}-http.rule=Host(`${var.fqdn}`)",
      ]

      port = "fe"


      check {
        name     = "${NOMAD_JOB_NAME} - alive"
        type     = "http"
        path     = "/"
        interval = "1m"
        timeout  = "10s"

        # Task should run 2m after deployment
        check_restart {
          limit           = 5
          grace           = "2m"
          ignore_warnings = true
        }
      }
    }

    task "nginx" {
      driver = "docker"

      config {
        image      = var.image
        force_pull = true
        ports      = ["fe"]
        labels {
          group = "backend"
        }
      }

      resources {
        cpu        = 100
        memory     = 32
        memory_max = 64
      }

    }

  } # END group frontend

}

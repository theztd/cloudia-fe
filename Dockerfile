FROM node:lts as build
COPY . /opt/
WORKDIR /opt/src
RUN npm install
RUN npm run build; ls -al; pwd



FROM nginx:alpine

COPY --from=build /opt/dist /usr/share/nginx/html

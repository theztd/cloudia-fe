import { createApp } from 'vue'

// import components
import './style.css'
import './dropdown.css'
import './assets/material-icons.css'

import Servers from './Servers.vue'
// import axios from 'axios'

// Bind servers component to servers id
const app = createApp(Servers)

// app.use(axios, {
//     baseUrl: "http://api-cloudia.fejk.net/v1"
// })
app.mount('#servers-app')